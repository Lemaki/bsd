var express = require('express');
var router = express.Router();
var database = require('../database');
/*
router.get('/', function(req,res,next){
   res.render('home'); 
});
*/
router.get('/', function(req, res, next) {
	var username = req.cookies.username;
    var hashedPassword = req.cookies.password;
    database.checkHashedPassword(username, hashedPassword,function(user){res.render('home_logged');},function(){res.render('home');});
});

module.exports = router;

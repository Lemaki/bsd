var express = require('express');
var router = express.Router();
var database = require('../database');

router.get('/', function(req, res, next){
    res.clearCookie('username'); 
    res.clearCookie('password'); 
    res.render('home');
});


module.exports = router;
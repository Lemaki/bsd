var express = require('express');
var router = express.Router();
var database = require('../database');

var usernameRegex = new RegExp(/[a-zA-Z]\w*/);

/* GET signup page. */
router.get('/', function(req, res, next) {
	res.render('signup');
});

/* POST signup information. */
router.post('/', function(req, res, next) {
	userUsername = req.body.username;
	userPassword = req.body.password;
	userCheckPassword = req.body.checkPassword;
	if ( userUsername === undefined || userPassword === undefined ) {
		res.render('signup', { error: "Missing username or password" });
	} else if (!usernameRegex.test(userUsername)) {
		res.render('signup', { error: "Invalid username, must start with a letter and only contain ASCII characters (a-z, A-Z, 0-9 and _)"});
	} else if (userPassword !== userCheckPassword) {
		res.render('signup', { error: "Passwords do not match" });
	} else {
		database.createUser(userUsername, userPassword, function(hashedPassword) {
			res.cookie('username', userUsername, {Path: '/'});
			res.cookie('password', hashedPassword, {Path: '/'});
			res.redirect('/');
		}, function() {
			res.render('signup', { error: "Username" + userUsername + "already taken" });
		});
	}
});

module.exports = router;

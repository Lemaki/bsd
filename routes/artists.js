var express = require('express');
var router = express.Router();
var mongo = require('mongodb');
var database = require('../database');


/* GET artists page. */
router.get('/', function(req, res, next) {
	username = req.cookies.username;
	hashedPassword = req.cookies.password;
	database.findArtist(req.query.artist, function(artist) {
		/* Page which lists all the artists unlocked by the user */
		if (artist === undefined) {
			database.checkHashedPassword(username, hashedPassword, function(user) {
				var artistList = user.unlockedArtists;
				res.render('artists', { artistList: artistList });
			}, function() {
				res.cookie('username', '', {Path: '/'});
				res.cookie('password', '', {Path: '/'});
				res.redirect('/');
			});
		/* Page which shows the details of a specific artist */
		} else {
			/*database.checkHashedPassword(username, hashedPassword, function(user) {
				var songList = user.unlocked;
				Array.prototype.addParagraphs = function self(list, callback) {
					function add(song) {
						if (song.artist === artist.name) {
							this.push(song.artist);
						}
					}
					while (list != []) {
						var songName = list.pop();
						database.findSong(songName, add);
					}
					callback(this);
				};
				[].addParagraphs(songList, function(paragraphList) {*/
					res.render('artist', { artist: artist/*, paragraphList: paragraphList*/});
			/*	});
			}, function() {
				res.cookie('username', '', {Path: '/'});
				res.cookie('password', '', {Path: '/'});
				res.redirect('/');
			});*/
		}
	});
});

module.exports = router;

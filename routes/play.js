var express = require('express');
var router = express.Router();
var database = require('../database');
var game = require('../game');

/* GET check if logged. */
router.get('/', function(req, res, next) {
    //var path = "sound/songs/hey_jude.mp3";
	var username = req.cookies.username;
    var hashedPassword = req.cookies.password;
    //var logged = false;
    database.checkHashedPassword(username, hashedPassword,function(user){ 
        var unlocked = user.unlocked;
        //console.log("unlocked créé");
        game.lockedList(unlocked,function(locked){
            //console.log("locked créé");
            game.randomSong(locked,function(song){
                if(song !== undefined){
                    res.render('play', {song : song});
                }else{
                    res.render('victory');
                }
                
            });
        });
    },function(){res.render('home');});

});

/* POST check answers */
router.post('/', function(req, res, next){
    var answer_artist = req.body.answer_artist;
    var answer_title = req.body.answer_title;
    var artist = req.body.artist;
    var title = req.body.title;
    var paragraph = req.body.paragraph;
    
    if(artist.toUpperCase() == answer_artist.toUpperCase() && title.toUpperCase() == answer_title.toUpperCase()){
        
        res.render('play_right', {paragraph : paragraph, title : answer_title, artist : answer_artist});
        var username = req.cookies.username;
        database.findSong(answer_title,function(song){
            console.log(song);
            database.unlockSong(username,song);
        });
    }else{
        res.render('play_wrong');
    } 
});

module.exports = router;
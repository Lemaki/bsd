var express = require('express');
var router = express.Router();
var database = require('../database');

/* GET login page. */
router.get('/', function(req, res, next) {
	res.render('login');
});

/* POST login information.  */
router.post('/', function(req, res, next) {
	userUsername = req.body.username;
	userPassword = req.body.password;
	if ( userUsername === undefined || userPassword === undefined ) {
		res.render('login', { error: "Missing username or password" });
	} else {
		database.checkPassword(userUsername, userPassword, function(userHashedPassword) {
			res.cookie('username', userUsername, {Path: '/'});
			res.cookie('password', userHashedPassword, {Path: '/'});
			res.redirect('/');
		}, function() {
			res.render('login', { error: "Invalid username or password" });
		});
	}
});

module.exports = router;

var crypto = require('crypto');
var mongo = require('mongodb');
var mongoClient = mongo.MongoClient;
var secret = "I love sloths.";


function contains(list, element) {
	for (var i = 0, len = list.length; i < len; i++) {
		if (list[i] == element) {
			return true;
		}
	}
	return false;
}


function encryptPassword(password, callback) {
	var hash = crypto.createHmac('sha256', secret).update(password).digest('hex');
	callback(hash);
}


function checkPassword(username, password, success, failure) {
	encryptPassword(password, function(hashedPassword) {
		mongoClient.connect('mongodb://localhost:27017/bsd', function(err, db) {
			if (err) {
				throw err;
			}
			var users = db.collection('users');
			users.find({username: username}).toArray(function(err, user) {
				if (err) {
					throw err;
				}
				if (user[0] !== undefined) {
					var dbPassword = user[0].password;
					db.close();
					if (dbPassword == hashedPassword) {
						success(hashedPassword);
					} else {
						// Password is invalid
						failure();
					}
				} else {
					// User doesn't exist
					failure();
				}
			});
		});
	});
}


function checkHashedPassword(username, hashedPassword, success, failure) {
	mongoClient.connect('mongodb://localhost:27017/bsd', function(err, db) {
		if (err) {
			throw err;
		}
		var users = db.collection('users');
		users.find({username: username}).toArray(function(err, user) {
			if (err) {
				throw err;
			}
			if (user[0] !== undefined) {
				var dbPassword = user[0].password;
				db.close();
				if (dbPassword == hashedPassword) {
					success(user[0]);
				} else {
					// Password is invalid
					failure();
				}
			} else {
				// User doesn't exist
				failure();
			}
		});
	});
}


function createUser(username, password, success, failure) {
	encryptPassword(password, function(hashedPassword) {
		mongoClient.connect('mongodb://localhost:27017/bsd', function(err, db) {
			if (err) {
				throw err;
			}
			var users = db.collection('users');
			users.find({username: username}).toArray(function(err, user) {
				if (user[0] !== undefined) {
					failure();
				} else {
					users.insert({username: username, password: hashedPassword, unlocked: [], unlockedArtists: []});
					db.close();
					success(hashedPassword);
				}
			});
		});
	});
}


function deleteUser(username) {
	mongoClient.connect('mongodb://localhost:27017/bsd', function(err, db) {
		if (err) {
			throw err;
		}
		var users = db.collection('users');
		users.deleteOne({username: username}, function(err, result) {
			if (err) {
				throw err;
			}
		});
	});
}


function unlockSong(username, song) {
	mongoClient.connect('mongodb://localhost:27017/bsd', function(err, db) {
		if (err) {
			throw err;
		}
		var users = db.collection('users');
		users.find({username : username}).toArray(function (err, user) {
				newUnlocked = user[0].unlocked;
				newUnlocked.push(song.title);
				newUnlockedArtists = user[0].unlockedArtists;
				if (!contains(newUnlockedArtists, song.artist)) {
					newUnlockedArtists.push(song.artist);
				}
				users.update({username: username}, {$set:{unlocked: newUnlocked, unlockedArtists: newUnlockedArtists}}, function(err, result) {
				if (err) {
					throw err;
				}
			});
		});
	});
}


function findSong(songName, callback) {
	mongoClient.connect('mongodb://localhost:27017/bsd', function(err, db) {
		if (err) {
			throw err;
		}
		var songs = db.collection('songs');
		songs.find().toArray(function (err, songArray) {
			if (err) {
				throw err;
			}
			songArray.forEach(function(song) {
				if (song !== null && song.title == songName) {
	                console.log(song);
					callback(song);
				}
			});
		});
	});
}


function findArtist(artistName, callback) {
	mongoClient.connect('mongodb://localhost:27017/bsd', function(err, db) {
		if (err) {
			throw err;
		}
		var artists = db.collection('artists');
		artists.find({name: artistName}).toArray(function (err, artist) {
			if (err) {
				throw err;
			}
			callback(artist[0]);
		});
	});
}

function fetchSongs(callback) {
	mongoClient.connect('mongodb://localhost:27017/bsd', function(err, db) {
		if (err) {
			throw err;
		}
		var songs = db.collection('songs');
		songs.find().toArray(function (err, songList) {
			if (err) {
				throw err;
			}
			callback(songList);
		});
	});
}

exports.encryptPassword = encryptPassword;
exports.checkPassword = checkPassword;
exports.checkHashedPassword = checkHashedPassword;
exports.createUser = createUser;
exports.deleteUser = deleteUser;
exports.unlockSong = unlockSong;
exports.findSong = findSong;
exports.findArtist = findArtist;
exports.fetchSongs = fetchSongs;

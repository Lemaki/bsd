var database = require('./database');


function lockedList(unlocked, callback) {
	database.fetchSongs(function(songList) {
        //console.log(songList);
		var finalList = songList.filter(function(song) {
			return unlocked.find(function(title) {
					return title === song.title;
				}) === undefined;
		});
        //console.log(finalList);
		callback(finalList);
	});
}


function randomSong(songList, callback) {
	var n = songList.length;
	var r = Math.random() * n;
	var i = r - r % 1;
	callback(songList[i]);
}


exports.lockedList = lockedList;
exports.randomSong = randomSong;
